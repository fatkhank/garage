﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Reston.DocMgt
{
    public class SearchFileQuery
    {
        /// <summary>
        /// Batasi pencarian hanya untuk objek dengan id yang tercantum.
        /// </summary>
        public Guid[] RestrictIds { get; set; }

        /// <summary>
        /// Batasi pencarian untuk objek yang ada dalam folder berikut.
        /// </summary>
        public FolderBase InsideFolder { get; set; }

        /// <summary>
        /// TypeId objek yang ingin dicari, null jika ingin mencari semua tipe.
        /// </summary>
        public string TypeId { get; set; }

        /// <summary>
        /// Quary pencarian
        /// </summary>
        public SQuery PropertyQuery { get; set; }
    }

    public abstract class SQuery
    {
        public static SQuery And(SQuery a, SQuery b)
        {
            if (a == null) {
                if (b == null) return null;
                else return b;
            }

            if (b == null) {
                if (a == null) return null;
                else return a;
            }
            return new QAnd(a, b);
        }
        public static SQuery Or(SQuery a, SQuery b)
        {
            if (a == null) {
                if (b == null) return null;
                else return b;
            }

            if (b == null) {
                if (a == null) return null;
                else return a;
            }
            return new QOr(a, b);
        }
    }

    public class QAnd : SQuery
    {
        internal SQuery Left { get; private set; }
        internal SQuery Right { get; private set; }

        internal QAnd(SQuery leftQuery, SQuery rightQuery) { Left = leftQuery; Right = rightQuery; }
    }

    public class QOr : SQuery
    {
        internal SQuery Left { get; private set; }
        internal SQuery Right { get; private set; }

        internal QOr(SQuery leftQuery, SQuery rightQuery) { Left = leftQuery; Right = rightQuery; }
    }

    public abstract class QValue : SQuery
    {
        internal string PropertyName { get; set; }
        internal QMode Mode { get; set; }

        internal enum QMode
        {
            NULL,
            NOT_NULL,
            TRUE,
            FALSE,
            CONTAINS,
            EQUALS,
            START_WITH,
            END_WITH,
            GREATER_THAN,
            GREATER_EQUALS,
            LESS_THAN,
            LESS_EQUALS,
        }
    }

    public class QString : QValue
    {
        internal string Value { get; private set; }
        protected QString() { }

        public static QString IsNull(string name) { return new QString() { PropertyName = name, Mode = QMode.NULL }; }
        public static QString IsNotNull(string name) { return new QString() { PropertyName = name, Mode = QMode.NOT_NULL }; }
        public static QString EqualsWith(string name, string value) { return new QString() { PropertyName = name, Value = value, Mode = QMode.EQUALS }; }
        public static QString Contains(string name, string value) { return new QString() { PropertyName = name, Value = value, Mode = QMode.CONTAINS }; }
        public static QString StartsWith(string name, string value) { return new QString() { PropertyName = name, Value = value, Mode = QMode.START_WITH }; }
        public static QString EndsWith(string name, string value) { return new QString() { PropertyName = name, Value = value, Mode = QMode.END_WITH }; }
    }

    public class QBool : QValue
    {
        private QBool() { }
        public static QBool True(string name) { return new QBool() { PropertyName = name, Mode = QMode.TRUE }; }
        public static QBool False(string name) { return new QBool() { PropertyName = name, Mode = QMode.FALSE }; }
        public static QBool IsNull(string name) { return new QBool() { PropertyName = name, Mode = QMode.NULL }; }
        public static QBool IsNotNull(string name) { return new QBool() { PropertyName = name, Mode = QMode.NOT_NULL }; }
    }

    public class QDate : QValue
    {
        internal DateTime Value { get; private set; }
        private QDate() { }

        public static QDate IsNull(string name) { return new QDate() { PropertyName = name, Mode = QMode.NULL }; }
        public static QDate IsNotNull(string name) { return new QDate() { PropertyName = name, Mode = QMode.NOT_NULL }; }
        public static QDate EqualsWith(DateTime value) { return new QDate() { Value = value, Mode = QMode.EQUALS }; }
        public static QDate GreaterThan(DateTime value) { return new QDate() { Value = value, Mode = QMode.GREATER_THAN }; }
        public static QDate GreaterOrEquals(DateTime value) { return new QDate() { Value = value, Mode = QMode.GREATER_EQUALS }; }
        public static QDate LessThan(DateTime value) { return new QDate() { Value = value, Mode = QMode.LESS_THAN }; }
        public static QDate LessOrEquals(DateTime value) { return new QDate() { Value = value, Mode = QMode.LESS_EQUALS }; }
    }

    public class QGuid : QValue
    {
        internal Guid Value { get; private set; }
        public static QGuid EqualsWith(string name, Guid value) { return new QGuid() { PropertyName = name, Value = value, Mode = QMode.EQUALS }; }
    }

    public class SearchOrder
    {
        internal string Name { get; private set; }
        internal bool IsDescending { get; private set; }

        public static SearchOrder Asc(string propertyName)
        {
            return new SearchOrder() { Name = propertyName, IsDescending = false };
        }

        public static SearchOrder Desc(string propertyName)
        {
            return new SearchOrder() { Name = propertyName, IsDescending = true };
        }

        private SearchOrder() { }
    }

    public class SearchResult
    {
        public IList<ObjectBase> Files { get; internal set; }
        public int TotalWithoutPaging { get; internal set; }
    }
}
