﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reston.DocMgt
{
    public abstract class ContentRepository : IDisposable
    {
        public const string LATEST_VERSION = "LAST_VERSION";

        /// <summary>
        /// Membuat file baru direpository. Setelah dibuat, property file akan diupdate sesuai dengan data di repo
        /// </summary>
        /// <param name="eofile"></param>
        /// <returns>Jika file sudah ada di repository atau gagal membuat file, akan mengembalikan null. Selainnya akan mengembalikan id file di repo</returns>
        public abstract Guid? CreateFile(FileBase eofile);

        /// <summary>
        /// Membuat folder baru direpository
        /// </summary>
        /// <param name="eofolder"></param>
        /// <returns></returns>
        public abstract Guid? CreateFolder(FolderBase eofolder);

        /// <summary>
        /// Menduplikat file di repository
        /// </summary>
        /// <param name="eofile"></param>
        /// <param name="allVersion">Salin semua versi content? (atau hanya yang terakhir).</param>
        /// <returns></returns>
        public abstract ObjectBase CloneFile(FileBase eofile, bool allVersion = true);


        public abstract ObjectBase FindObject(Guid id);

        /// <summary>
        /// Menyimpan perubahan terhadap objek ke repo. Objek harus sudah dicreate terlebih dahulu sebelum diupdate.
        /// </summary>
        /// <param name="eofile"></param>
        /// <returns>berhasil menyimpan atau tidak</returns>
        public abstract bool UpdateFile(FileBase eofile);

        public abstract bool DeleteObject(ObjectBase eobject);

        public abstract HashSet<string> GetPermission(ObjectBase eobject, string principalId);
        public abstract void AddPermission(ObjectBase eobject, string principalId, string permission);
        public abstract void RemovePermission(ObjectBase eobject, string principalId, string permission);

        public abstract bool TryLock(ObjectBase eobject);
        public abstract bool TryUnlock(ObjectBase eobject);
        public abstract bool IsLocked(ObjectBase eobject);

        public abstract void Dispose();

        //-----------------------------------------------------------------
        public abstract FolderBase GetDocumentsFolder(string username = null);
        public abstract FolderBase GetTemplatesFolder(string username = null);
        public abstract FolderBase GetTemporaryFolder(string username);
        public abstract FolderBase GetMapsFolder();

        /// <summary>
        /// Memasukkan objek ke folder, objek dan folder harus sudah dicreate terlebih dahulu. Jika sudah ada dalam folder.
        /// </summary>
        /// <param name="eobject"></param>
        /// <param name="folder"></param>
        public abstract bool AddToFolder(ObjectBase eobject, FolderBase folder);

        /// <summary>
        /// Menghapus objek dari folder.
        /// </summary>
        /// <param name="eobject"></param>
        /// <param name="folder"></param>
        public abstract void RemoveFromFolder(ObjectBase eobject, FolderBase folder);

        /// <summary>
        /// Persiapkan penyimpanan repository
        /// </summary>
        /// <param name="username">Persiapkan penyimpanan milik user tertentu, atau jika null akan menyiapkan seluruh repo.</param>
        public abstract void PrepareWorkspace(string username = null);

        /// <summary>
        /// Mencari file menggunakan query
        /// </summary>
        /// <param name="query"></param>
        /// <param name="properties"></param>
        /// <param name="order"></param>
        /// <param name="skip"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public abstract SearchResult SearchObject(SearchFileQuery query, ICollection<string> properties, SearchOrder[] order = null, int? skip = null, int count = int.MaxValue);
    }
}
