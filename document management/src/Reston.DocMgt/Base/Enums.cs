﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocMgt.Reston
{
    [RefData("PermissionAction")]
    public static class PermissionAction
    {
        /// <summary>
        /// Melihat bagian dokumen
        /// </summary>
        [RefDataEntry(Desc = "Melihat isi dokumen")]
        public const string VIEW = "View";
        /// <summary>
        /// Memodifikasi isi dokumen
        /// </summary>
        [RefDataEntry(Desc = "Mengubah isi dokumen")]
        public const string MODIFY = "Modify";
        /// <summary>
        /// Memberikan approval
        /// </summary>
        [RefDataEntry(Desc = "Memberikan approval")]
        public const string APPROVAL = "Approval";
        /// <summary>
        /// Menghapus isi dari dokumen
        /// </summary>
        [RefDataEntry(Desc = "Menghapus isi dari dokumen")]
        public const string DELETE_CONTENT = "DeleteContent";
    }
}
