﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reston.DocMgt
{
    public abstract class ObjectBase
    {
        protected const string TYPE_ID_PREFIX = "reston:";

        public Guid Id { get; internal set; }

        public string Name { get; set; }

        internal Guid CreatedBy { get; set; }
        public DateTime CreatedOn { get; internal set; }

        public Guid? LastModifiedBy { get; internal set; }
        public DateTime? LastModifiedOn { get; internal set; }

        private const string PROPERTY_PREFIX = "EObject:";

        public const string PROPERTY_ID = PROPERTY_PREFIX + "Id";
        public const string PROPERTY_TYPEID = PROPERTY_PREFIX + "TypeId";
        public const string PROPERTY_CREATED_BY = PROPERTY_PREFIX + "CreatedBy";
        public const string PROPERTY_CREATED_ON = PROPERTY_PREFIX + "CreatedOn";
        public const string PROPERTY_LAST_MODIFIED_BY = PROPERTY_PREFIX + "LastModifiedBy";
        public const string PROPERTY_LAST_MODIFIED_ON = PROPERTY_PREFIX + "LastModifiedOn";

        private Dictionary<string, string> _StringProperties;
        protected internal Dictionary<string, string> StringProperties
        {
            get
            {
                if (_StringProperties == null) _StringProperties = new Dictionary<string, string>();
                return _StringProperties;
            }
        }
        public string GetStringProperty(string name)
        {
            return StringProperties.ContainsKey(name) ? StringProperties[name] : null;
        }

        private Dictionary<string, int> _IntProperties;
        protected internal Dictionary<string, int> IntProperties
        {
            get
            {
                if (_IntProperties == null) _IntProperties = new Dictionary<string, int>();
                return _IntProperties;
            }
        }
        public int? GetIntProperty(string name)
        {
            return IntProperties.ContainsKey(name) ? IntProperties[name] : (int?)null;
        }

        private Dictionary<string, bool> _BoolProperties;
        protected internal Dictionary<string, bool> BoolProperties
        {
            get
            {
                if (_BoolProperties == null) _BoolProperties = new Dictionary<string, bool>();
                return _BoolProperties;
            }
        }
        public bool? GetBooleanProperty(string name)
        {
            return BoolProperties.ContainsKey(name) ? BoolProperties[name] : (bool?) null;
        }

        private Dictionary<string, DateTime> _DatetimeProperties;
        protected internal Dictionary<string, DateTime> DateTimeProperties
        {
            get
            {
                if (_DatetimeProperties == null) _DatetimeProperties = new Dictionary<string, DateTime>();
                return _DatetimeProperties;
            }
        }
        public DateTime? GetDateTimeProperty(string name)
        {
            return DateTimeProperties.ContainsKey(name) ? DateTimeProperties[name] : (DateTime?) null;
        }

        public void SetProperty(string name, object value)
        {
            if (value is string) StringProperties[name] = (string)value;
            else if (value is int) IntProperties[name] = (int)value;
            else if (value is Boolean) BoolProperties[name] = (bool)value;
            else if (value is DateTime) DateTimeProperties[name] = (DateTime)value;
        }

        protected internal Dictionary<string, object> Extensions { get; set; }

        protected internal Func<ObjectBase, Guid[]> GetParentFoldersFunc { get; set; }
        public Guid[] GetParentFolders()
        {
            if (GetParentFoldersFunc == null) return new Guid[0];
            return GetParentFoldersFunc(this);
        }

        public ObjectBase() { }
    }

    public class FolderBase : ObjectBase
    {
        public virtual bool Accept(ObjectBase eobject) { return true; }
        internal Func<FolderBase, Guid[]> GetChildrenFunc { get; set; }
        public Guid[] GetChildren()
        {
            if (GetChildrenFunc == null) return new Guid[0];
            return GetChildrenFunc(this);
        }
    }

    public abstract class FileBase : ObjectBase
    {
        public string Version { get; internal set; }
        public string ContentType { get; internal set; }

        public abstract bool ReadContentFrom(Stream stream);
        public abstract void WriteContentTo(Stream stream);
    }
}
