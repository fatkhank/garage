﻿using System;
using System.Data.Entity;
using System.Linq;

namespace Reston.DocMgt.DBRepo
{
    internal partial class DBRepoEntities : DbContext
    {
        /// <summary>
        /// Nama skema untuk database modul dokumen
        /// </summary>
        public const string SCHEMA_NAME = "doc.repo";

        public DBRepoEntities() : base("Name="+DbBasedContentRepository.ConnectionStringName) { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            Configuration.LazyLoadingEnabled = true;

            modelBuilder.Entity<RepoFolder>()
                .HasMany(a => a.Children)
                .WithMany(a => a.ParentFolders)
                .Map(a => a.ToTable("FolderContents", SCHEMA_NAME));
        }

        public virtual DbSet<RepoFolder> Folders { get; set; }
        public virtual DbSet<RepoSpecialFolder> SpecialFolders { get; set; }

        public virtual DbSet<RepoFile> Files { get; set; }

        public virtual DbSet<RepoACE> ACEs { get; set; }

        internal Guid accessorUID { get; set; }

        /// <summary>
        /// Cantumkan nama user yang sedang mengakses entiti.
        /// </summary>
        /// <param name="username"></param>
        public void SetUser(Guid userid)
        {
            accessorUID = userid;
        }

        /// <summary>
        /// Menyimpan perubahan ke database. Jika terdapat perubahan di ReferenceData, pastikan <code>ValidateReferenceData</code> dipanggil terlebih dahulu.
        /// </summary>
        /// <returns></returns>
        public override int SaveChanges()
        {
            //cantumkan audit trail untuk entity yang ICreationAuditable
            foreach (var entry in this.ChangeTracker.Entries<ICreationAuditable>().Where(a => a.State == EntityState.Added)) {
                entry.Entity.CreatedByUID = accessorUID;
                entry.Entity.CreatedOn = DateTime.Now;
            }

            //cantumkan audit trail untuk entity yang IModificationAuditable
            foreach (var entry in this.ChangeTracker.Entries<IModificationAuditable>().Where(a => a.State == EntityState.Modified)) {
                entry.Entity.LastUpdatedByUID = accessorUID;
                entry.Entity.LastUpdatedOn = DateTime.Now;
            }

            //cantumkan audit trail untuk entity yang ISoftDeletable
            foreach (var entry in this.ChangeTracker.Entries<ISoftDeleteable>().Where(a => a.State == EntityState.Deleted)) {
                entry.Entity.DeletedByUID = accessorUID;
                entry.Entity.DeletedOn = DateTime.Now;
                entry.Entity.IsDeleted = true;
                entry.State = EntityState.Modified;
            }

            return base.SaveChanges();
        }

        public override System.Threading.Tasks.Task<int> SaveChangesAsync()
        {
            //cantumkan audit trail untuk entity yang ICreationAuditable
            foreach (var entry in this.ChangeTracker.Entries<ICreationAuditable>().Where(a => a.State == EntityState.Added)) {
                entry.Entity.CreatedByUID = accessorUID;
                entry.Entity.CreatedOn = DateTime.Now;
            }

            //cantumkan audit trail untuk entity yang IModificationAuditable
            foreach (var entry in this.ChangeTracker.Entries<IModificationAuditable>().Where(a => a.State == EntityState.Modified)) {
                entry.Entity.LastUpdatedByUID = accessorUID;
                entry.Entity.LastUpdatedOn = DateTime.Now;
            }

            //cantumkan audit trail untuk entity yang ISoftDeletable
            foreach (var entry in this.ChangeTracker.Entries<ISoftDeleteable>().Where(a => a.State == EntityState.Deleted)) {
                entry.Entity.DeletedByUID = accessorUID;
                entry.Entity.DeletedOn = DateTime.Now;
                entry.Entity.IsDeleted = true;
                entry.State = EntityState.Modified;
            }
            return base.SaveChangesAsync();
        }

        /// <summary>
        /// Menyaring dari dbset yang belum dihapus
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dbset"></param>
        /// <returns></returns>
        public IQueryable<T> AliveFrom<T>(DbSet<T> dbset) where T : CreationDeletionAuditableBaseEntity
        {
            return dbset.Where(a => !a.IsDeleted);
        }

        /// <summary>
        /// Mencari entry dalam set yang belum dihapus
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dbset"></param>
        /// <param name="keyValues"></param>
        /// <returns></returns>
        public T FindAlive<T>(DbSet<T> dbset, params object[] keyValues) where T : CreationDeletionAuditableBaseEntity
        {
            T entry = dbset.Find(keyValues);
            if (entry == null || entry.IsDeleted) return default(T);//jika tidak ketemu atau sudah dihapus, kembalikan null
            else return entry;
        }
    }
}
