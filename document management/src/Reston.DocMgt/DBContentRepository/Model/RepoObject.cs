﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;

namespace Reston.DocMgt.DBRepo
{
    [Table(TABLE_NAME, Schema = DBRepoEntities.SCHEMA_NAME)]
    public abstract class RepoObject : CUDAuditableBaseEntity
    {
        public const string TABLE_NAME = "RepoObjects";

        [Key]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public string TypeId { get; set; }

        public string Name { get; set; }

        public bool IsLocked { get; set; }

        public virtual ICollection<RepoACE> ACL { get; set; }

        public virtual ICollection<StringProperty> StringProperties { get; set; }
        public virtual ICollection<IntProperty> IntProperties { get; set; }
        public virtual ICollection<BooleanProperty> BooleanProperties { get; set; }
        public virtual ICollection<DateTimeProperty> DateTimeProperties { get; set; }

        [InverseProperty("Children")]
        public virtual ICollection<RepoFolder> ParentFolders { get; set; }
    }

    [Table(TABLE_NAME, Schema = DBRepoEntities.SCHEMA_NAME)]
    public class RepoFile : RepoObject
    {
        public new const string TABLE_NAME = "Files";
        public string ContentType { get; set; }
        public byte[] Content { get; set; }
        public string VersionLabel { get; set; }

        public virtual RepoFile PreviousVersion { get; set; }
    }

    [Table(TABLE_NAME, Schema = DBRepoEntities.SCHEMA_NAME)]
    public class RepoFolder : RepoObject
    {
        public new const string TABLE_NAME = "Folders";

        public virtual ICollection<RepoObject> Children { get; set; }
    }

    [Table(TABLE_NAME, Schema = DBRepoEntities.SCHEMA_NAME)]
    public class RepoSpecialFolder : RepoFolder
    {
        public new const string TABLE_NAME = "SpecialFolders";

        [Index]
        [MaxLength(300)]
        public string SpecialName { get; set; }
    }

    [Table(TABLE_NAME, Schema = DBRepoEntities.SCHEMA_NAME)]
    public class RepoACE
    {
        public const string TABLE_NAME = "ACL";
        public const string INDEX_OBJECT_PRINCIPALID = "Index_RepoObject_PrincipalID";

        [Key]
        public int Id { get; set; }

        [Index(INDEX_OBJECT_PRINCIPALID, Order = 1)]
        [Required]
        public virtual RepoObject OwnerObject { get; set; }

        [Index(INDEX_OBJECT_PRINCIPALID, Order = 2)]
        [Required]
        [MaxLength(64)]
        public string PrincipalId { get; set; }

        public string Permissions { get; set; }

        const char PERMISSION_SEPARATOR = '|';
        public void AddPermission(string permission)
        {
            if (string.IsNullOrEmpty(permission)) return;
            if (string.IsNullOrEmpty(Permissions)) Permissions = PERMISSION_SEPARATOR.ToString();
            string pattern = permission + PERMISSION_SEPARATOR;
            //jika belum ada, baru masukkan
            if (Permissions.IndexOf(PERMISSION_SEPARATOR + pattern) < 0) {
                Permissions += pattern;
            }
        }

        public void RemovePermission(string permission)
        {
            if (!string.IsNullOrEmpty(Permissions)) {
                string pattern = permission + PERMISSION_SEPARATOR;
                int index = permission.IndexOf(PERMISSION_SEPARATOR + pattern);
                //jika ditemukan, buang permission
                if (index >= 0) {
                    permission = permission.Remove(index, pattern.Length);
                }
            }
        }

        public string[] GetPermissions()
        {
            if (string.IsNullOrEmpty(Permissions)) return new string[0];
            return Permissions.Substring(1, Permissions.Length - 2).Split(PERMISSION_SEPARATOR);
        }

        public bool HasPermission(string permission)
        {
            if (string.IsNullOrEmpty(Permissions)) return false;
            return Permissions.IndexOf(PERMISSION_SEPARATOR + permission + PERMISSION_SEPARATOR) >= 0;
        }
    }
}
