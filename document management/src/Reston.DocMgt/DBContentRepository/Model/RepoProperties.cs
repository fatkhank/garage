﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reston.DocMgt.DBRepo
{
    public abstract class RepoObjectProperty : CreationAuditableBaseEntity
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public RepoObject OwningObject { get; set; }

        [Required]
        public string Name { get; set; }
    }

    [Table(TABLE_NAME, Schema = DBRepoEntities.SCHEMA_NAME)]
    public class StringProperty : RepoObjectProperty
    {
        public const string TABLE_NAME = "StringProperties";
        public string Value { get; set; }
    }

    [Table(TABLE_NAME, Schema = DBRepoEntities.SCHEMA_NAME)]
    public class IntProperty : RepoObjectProperty
    {
        public const string TABLE_NAME = "IntProperties";
        public int Value { get; set; }
    }

    [Table(TABLE_NAME, Schema = DBRepoEntities.SCHEMA_NAME)]
    public class BooleanProperty : RepoObjectProperty
    {
        public const string TABLE_NAME = "BooleanProperties";
        public bool Value { get; set; }
    }

    [Table(TABLE_NAME, Schema = DBRepoEntities.SCHEMA_NAME)]
    public class DateTimeProperty : RepoObjectProperty
    {
        public const string TABLE_NAME = "DateTimeProperties";
        public DateTime Value { get; set; }
    }
}
