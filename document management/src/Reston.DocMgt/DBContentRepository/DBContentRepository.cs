﻿using System;
using System.Linq;
using System.Data.Entity;
using System.Collections.Generic;
using Iface = Reston.DocMgt;
using System.IO;
using System.Linq.Expressions;

namespace Reston.DocMgt
{
    public class DbBasedContentRepository : ContentRepository
    {
        public static string ConnectionStringName;

        protected const string EXTENSION_REPO_OBJECT = "RepoObject";
        protected const string VERSION_NEW = "1.0.0";

        private DBRepo.DBRepoEntities db;

        public DbBasedContentRepository(Guid userid)
        {
            db = new DBRepo.DBRepoEntities();
            db.SetUser(userid);
        }

        internal DbBasedContentRepository(DBRepo.DBRepoEntities context)
        {
            this.db = context;
        }

        public override Guid? CreateFolder(Iface.FolderBase eofolder)
        {
            //jika sudah dibuat, abaikan
            if (eofolder.Id != Guid.Empty) return null;

            //buat folder
            var createdFolder = db.Folders.Add(new DBRepo.RepoFolder() {
                Name = eofolder.Name
            });

            createdFolder.TypeId = DBRepo.TypeResolver.GetTypeId(eofolder);

            //simpan properties
            AddPropertiesToDb(createdFolder, eofolder);

            db.SaveChanges();

            //reload objek yang baru saja dimasukkan
            db.Entry(createdFolder).Reload();
            eofolder.Id = createdFolder.Id;
            eofolder.CreatedOn = createdFolder.CreatedOn;
            eofolder.CreatedBy = createdFolder.CreatedByUID;
            eofolder.Extensions = new Dictionary<string, object> { { EXTENSION_REPO_OBJECT, createdFolder } };
            eofolder.GetChildrenFunc = getFolderChildren;
            eofolder.GetParentFoldersFunc = getParentFolders;

            return createdFolder.Id;
        }

        protected void AddPropertiesToDb(DBRepo.RepoObject owningObject, Iface.ObjectBase sourceObject)
        {
            //string
            if (owningObject.StringProperties == null) owningObject.StringProperties = new List<DBRepo.StringProperty>();
            foreach (var property in sourceObject.StringProperties) {
                owningObject.StringProperties
                    //db.Set<DBRepo.StringProperty>()
                    .Add(new DBRepo.StringProperty() {
                        OwningObject = owningObject,
                        Name = property.Key,
                        Value = property.Value
                    });
            }
            //int
            if (owningObject.IntProperties == null) owningObject.IntProperties = new List<DBRepo.IntProperty>();
            foreach (var property in sourceObject.IntProperties) {
                owningObject.IntProperties
                    //db.Set<DBRepo.IntProperty>()
                    .Add(new DBRepo.IntProperty() {
                        OwningObject = owningObject,
                        Name = property.Key,
                        Value = property.Value
                    });
            }
            //bool
            if (owningObject.BooleanProperties == null) owningObject.BooleanProperties = new List<DBRepo.BooleanProperty>();
            foreach (var property in sourceObject.BoolProperties) {
                owningObject.BooleanProperties
                    //db.Set<DBRepo.BooleanProperty>()
                    .Add(new DBRepo.BooleanProperty() {
                        OwningObject = owningObject,
                        Name = property.Key,
                        Value = property.Value
                    });
            }
            //datetime
            if (owningObject.DateTimeProperties == null) owningObject.DateTimeProperties = new List<DBRepo.DateTimeProperty>();
            foreach (var property in sourceObject.DateTimeProperties) {
                owningObject.DateTimeProperties
                    //db.Set<DBRepo.DateTimeProperty>()
                    .Add(new DBRepo.DateTimeProperty() {
                        OwningObject = owningObject,
                        Name = property.Key,
                        Value = property.Value
                    });
            }
        }

        protected void DuplicateProperties(DBRepo.RepoObject owningObject, DBRepo.RepoObject sourceObject)
        {
            //string
            foreach (var property in sourceObject.StringProperties) {
                db.Set<DBRepo.StringProperty>().Add(new DBRepo.StringProperty() {
                    OwningObject = owningObject,
                    Name = property.Name,
                    Value = property.Value
                });
            }
            //int
            foreach (var property in sourceObject.IntProperties) {
                db.Set<DBRepo.IntProperty>().Add(new DBRepo.IntProperty() {
                    OwningObject = owningObject,
                    Name = property.Name,
                    Value = property.Value
                });
            }
            //bool
            foreach (var property in sourceObject.BooleanProperties) {
                db.Set<DBRepo.BooleanProperty>().Add(new DBRepo.BooleanProperty() {
                    OwningObject = owningObject,
                    Name = property.Name,
                    Value = property.Value
                });
            }
            //datetime
            foreach (var property in sourceObject.DateTimeProperties) {
                db.Set<DBRepo.DateTimeProperty>().Add(new DBRepo.DateTimeProperty() {
                    OwningObject = owningObject,
                    Name = property.Name,
                    Value = property.Value
                });
            }
        }

        public override Guid? CreateFile(Iface.FileBase eofile)
        {
            //jika sudah dibuat, abaikan
            if (eofile.Id != Guid.Empty) return null;

            //buat file
            var createdFile = db.Files.Add(new DBRepo.RepoFile() {
                Name = eofile.Name,
                VersionLabel = VERSION_NEW
            });

            //ambil content type
            createdFile.TypeId = DBRepo.TypeResolver.GetTypeId(eofile);
            createdFile.ContentType = DBRepo.TypeResolver.GetContentType(eofile);
            eofile.ContentType = createdFile.ContentType;

            //salin isi file
            using (var bufferStream = new MemoryStream()) {
                eofile.WriteContentTo(bufferStream);
                createdFile.Content = bufferStream.ToArray();
            }

            //simpan properties
            AddPropertiesToDb(createdFile, eofile);

            db.SaveChanges();

            //reload objek yang baru saja dimasukkan
            db.Entry(createdFile).Reload();

            eofile.Id = createdFile.Id;
            eofile.CreatedOn = createdFile.CreatedOn;
            eofile.CreatedBy = createdFile.CreatedByUID;
            eofile.Extensions = new Dictionary<string, object> { { EXTENSION_REPO_OBJECT, createdFile } };
            eofile.GetParentFoldersFunc = getParentFolders;

            return createdFile.Id;
        }


        public override Iface.ObjectBase CloneFile(Iface.FileBase eofile, bool allVersion = true)
        {
            //jika belum di add, tidak bisa diclone
            if (eofile.Id == Guid.Empty) return null;

            //simpan kalau sebagai file
            var repoFile = (DBRepo.RepoFile)eofile.Extensions[EXTENSION_REPO_OBJECT];

            //duplikat file
            var clonedFile = db.Files.Add(new DBRepo.RepoFile() {
                Name = repoFile.Name,
                IsLocked = repoFile.IsLocked,
                Content = repoFile.Content,
                ContentType = repoFile.ContentType,
                VersionLabel = VERSION_NEW
            });

            //duplikat properties
            DuplicateProperties(clonedFile, clonedFile);

            if (allVersion) {
                clonedFile.VersionLabel = repoFile.VersionLabel;

                //clone versi2 sebelumnya
                DBRepo.RepoFile currentSource = repoFile.PreviousVersion, currentCloned = clonedFile;
                while (currentSource != null) {
                    currentCloned.PreviousVersion = db.Files.Add(new DBRepo.RepoFile() {
                        Name = currentSource.Name,
                        IsLocked = currentSource.IsLocked,
                        Content = currentSource.Content,
                        ContentType = currentSource.ContentType,
                        VersionLabel = currentSource.VersionLabel
                    });

                    currentCloned = currentCloned.PreviousVersion;
                    //salin property
                    DuplicateProperties(currentCloned, currentSource);

                    currentSource = currentSource.PreviousVersion;
                }
            }

            db.SaveChanges();
            return WrapFile(clonedFile);
        }

        private Guid[] getFolderChildren(Iface.FolderBase eofolder)
        {
            var repoFolder = (DBRepo.RepoFolder)eofolder.Extensions[EXTENSION_REPO_OBJECT];
            return repoFolder.Children.Select(a => a.Id).ToArray();
        }

        private Guid[] getParentFolders(Iface.ObjectBase eobject)
        {
            var repoObject = (DBRepo.RepoObject)eobject.Extensions[EXTENSION_REPO_OBJECT];
            return repoObject.ParentFolders.Select(a => a.Id).ToArray();
        }

        public override Iface.ObjectBase FindObject(Guid id)
        {
            //--- coba cari di file
            var existingObject = db.FindAlive(db.Set<DBRepo.RepoObject>(), id);
            if (existingObject != null) {
                if (existingObject is DBRepo.RepoFile) {
                    var existingFile = (DBRepo.RepoFile)existingObject;
                    if (existingFile != null) return WrapFile(existingFile);
                } else {
                    //--- coba cari di folder
                    var existingFolder = (DBRepo.RepoFolder)existingObject;
                    if (existingFolder != null) {
                        var eofolder = (Iface.FolderBase)DBRepo.TypeResolver.CreateInstance(existingFolder);

                        eofolder.Id = existingFolder.Id;
                        eofolder.Name = existingFolder.Name;
                        eofolder.CreatedOn = existingFolder.CreatedOn;
                        eofolder.CreatedBy = existingFolder.CreatedByUID;
                        eofolder.LastModifiedOn = existingFolder.LastUpdatedOn;
                        eofolder.LastModifiedBy = existingFolder.LastUpdatedByUID;
                        eofolder.Extensions = new Dictionary<string, object> { { EXTENSION_REPO_OBJECT, existingFolder } };
                        eofolder.GetChildrenFunc = getFolderChildren;
                        eofolder.GetParentFoldersFunc = getParentFolders;

                        //baca properti
                        if (existingFolder.StringProperties != null) db.Entry(existingFolder).Collection(a => a.StringProperties).Load();
                        foreach (var property in existingFolder.StringProperties) {
                            eofolder.StringProperties[property.Name] = property.Value;
                        }

                        if (existingFolder.IntProperties != null) db.Entry(existingFolder).Collection(a => a.IntProperties).Load();
                        foreach (var property in existingFolder.IntProperties) {
                            eofolder.IntProperties[property.Name] = property.Value;
                        }

                        if (existingFolder.BooleanProperties != null) db.Entry(existingFolder).Collection(a => a.BooleanProperties).Load();
                        foreach (var property in existingFolder.BooleanProperties) {
                            eofolder.BoolProperties[property.Name] = property.Value;
                        }

                        if (existingFolder.DateTimeProperties != null) db.Entry(existingFolder).Collection(a => a.DateTimeProperties).Load();
                        foreach (var property in existingFolder.DateTimeProperties) {
                            eofolder.DateTimeProperties[property.Name] = property.Value;
                        }

                        return eofolder;
                    }
                }
            }

            return null;
        }

        private Iface.ObjectBase WrapFile(DBRepo.RepoFile repoFile)
        {
            Iface.FileBase wrapper = (Iface.FileBase)DBRepo.TypeResolver.CreateInstance(repoFile);

            //jika tidak punya isi, berikan file kosong
            if (repoFile.Content != null) {    //baca content
                wrapper.ContentType = repoFile.ContentType;
                using (var reader = new MemoryStream(repoFile.Content)) {
                    wrapper.ReadContentFrom(reader);
                }

                //baca properti
                foreach (var property in repoFile.StringProperties) {
                    wrapper.StringProperties[property.Name] = property.Value;
                }
                foreach (var property in repoFile.IntProperties) {
                    wrapper.IntProperties[property.Name] = property.Value;
                }
                foreach (var property in repoFile.BooleanProperties) {
                    wrapper.BoolProperties[property.Name] = property.Value;
                }
                foreach (var property in repoFile.DateTimeProperties) {
                    wrapper.DateTimeProperties[property.Name] = property.Value;
                }
            }

            //baca properti
            wrapper.Id = repoFile.Id;
            wrapper.Name = repoFile.Name;
            wrapper.CreatedOn = repoFile.CreatedOn;
            wrapper.CreatedBy = repoFile.CreatedByUID;
            wrapper.LastModifiedOn = repoFile.LastUpdatedOn;
            wrapper.LastModifiedBy = repoFile.LastUpdatedByUID;
            wrapper.Extensions = new Dictionary<string, object> { { EXTENSION_REPO_OBJECT, repoFile } };
            wrapper.GetParentFoldersFunc = getParentFolders;

            return wrapper;
        }

        public override bool UpdateFile(Iface.FileBase eofile)
        {
            var repoFile = (DBRepo.RepoFile)eofile.Extensions[EXTENSION_REPO_OBJECT];
            if (repoFile == null) return false;

            //update isi content
            repoFile.ContentType = eofile.ContentType;
            using (var bufferStream = new MemoryStream()) {
                eofile.WriteContentTo(bufferStream);
                repoFile.Content = bufferStream.ToArray();
            }

            //simpan properti baru
            AddPropertiesToDb(repoFile, eofile);
            repoFile.LastUpdatedByUID = db.accessorUID;
            repoFile.LastUpdatedOn = DateTime.Now;

            db.SaveChanges();
            if (repoFile != null) {
                eofile.LastModifiedBy = repoFile.LastUpdatedByUID;
                eofile.LastModifiedOn = repoFile.LastUpdatedOn;
            }

            return true;
        }

        public override bool DeleteObject(Iface.ObjectBase eobject)
        {
            if (eobject is Iface.FolderBase) {
                var folder = db.FindAlive(db.Folders, eobject.Id);
                if (folder == null) return false;
                db.Folders.Remove(folder);
            } else if (eobject is Iface.FileBase) {
                var file = db.FindAlive(db.Files, eobject.Id);
                if (file == null) return false;
                db.Files.Remove(file);
            }

            db.SaveChanges();
            return true;
        }

        public override HashSet<string> GetPermission(Iface.ObjectBase eobject, string principalId)
        {
            var repoObject = (DBRepo.RepoObject)eobject.Extensions[EXTENSION_REPO_OBJECT];

            var aclNav = db.Entry(repoObject).Collection(a => a.ACL);
            if (!aclNav.IsLoaded) aclNav.Load();

            var acl = repoObject.ACL.FirstOrDefault(a => a.PrincipalId == principalId);
            if (acl != null) return new HashSet<string>(acl.GetPermissions());
            return new HashSet<string>();
        }

        public override void AddPermission(Iface.ObjectBase eobject, string principalId, string permission)
        {
            var repoObject = (DBRepo.RepoObject)eobject.Extensions[EXTENSION_REPO_OBJECT];

            //pastikan acl diload
            var aclNav = db.Entry(repoObject).Collection(a => a.ACL);
            if (!aclNav.IsLoaded) aclNav.Load();

            var ace = repoObject.ACL.FirstOrDefault(a => a.PrincipalId == principalId);
            if (ace == null) {
                ace = db.ACEs.Add(new DBRepo.RepoACE() {
                    OwnerObject = repoObject,
                    PrincipalId = principalId
                });
            }

            ace.AddPermission(permission);

            db.SaveChanges();
        }

        public override void RemovePermission(Iface.ObjectBase eobject, string principalId, string permission)
        {
            var repoObject = (DBRepo.RepoObject)eobject.Extensions[EXTENSION_REPO_OBJECT];

            //pastikan acl diload
            var aclNav = db.Entry(repoObject).Collection(a => a.ACL);
            if (!aclNav.IsLoaded) aclNav.Load();

            var ace = repoObject.ACL.FirstOrDefault(a => a.PrincipalId == principalId);
            if (ace != null) {
                ace.RemovePermission(permission);
                db.SaveChanges();
            }
        }

        public override bool TryLock(Iface.ObjectBase eobject)
        {
            var repoObject = (DBRepo.RepoObject)eobject.Extensions[EXTENSION_REPO_OBJECT];
            //already locked
            if (repoObject.IsLocked) return false;
            repoObject.IsLocked = true;
            db.SaveChanges();
            return true;
        }

        public override bool TryUnlock(Iface.ObjectBase eobject)
        {
            var repoObject = (DBRepo.RepoObject)eobject.Extensions[EXTENSION_REPO_OBJECT];
            repoObject.IsLocked = false;
            db.SaveChanges();
            return true;
        }

        public override bool IsLocked(Iface.ObjectBase eobject)
        {
            var repoObject = (DBRepo.RepoObject)eobject.Extensions[EXTENSION_REPO_OBJECT];
            return repoObject.IsLocked;
        }

        public override void Dispose()
        {
            db.Dispose();
        }

        public override bool AddToFolder(Iface.ObjectBase eobject, Iface.FolderBase folder)
        {
            if (folder == null || !folder.Accept(eobject)) return false;

            var repoFolder = (DBRepo.RepoFolder)folder.Extensions[EXTENSION_REPO_OBJECT];
            var repoObject = (DBRepo.RepoObject)eobject.Extensions[EXTENSION_REPO_OBJECT];
            if (repoFolder != null && repoObject != null) {
                db.Entry(repoFolder).Collection(a => a.Children).Load();
                repoFolder.Children.Add(repoObject);
                repoFolder.LastUpdatedOn = DateTime.Now;
                repoFolder.LastUpdatedByUID = db.accessorUID;

                db.SaveChanges();
            }
            return true;
        }

        public override void RemoveFromFolder(Iface.ObjectBase eobject, Iface.FolderBase folder)
        {
            var repoFolder = (DBRepo.RepoFolder)folder.Extensions[EXTENSION_REPO_OBJECT];
            var repoObject = (DBRepo.RepoObject)eobject.Extensions[EXTENSION_REPO_OBJECT];
            if (repoFolder != null && repoObject != null) {
                db.Entry(repoFolder).Collection(a => a.Children).Load();
                repoFolder.Children.Remove(repoObject);
                repoFolder.LastUpdatedByUID = db.accessorUID;
                repoFolder.LastUpdatedOn = DateTime.Now;
                db.SaveChanges();
            }
        }

        private string GetDirectField(string name)
        {
            switch (name) {
                case Iface.ObjectBase.PROPERTY_CREATED_BY:
                    return "CreatedByUID";
                case Iface.ObjectBase.PROPERTY_CREATED_ON:
                    return "CreatedOn";
                case Iface.ObjectBase.PROPERTY_LAST_MODIFIED_BY:
                    return "LastModifiedByUID";
                case Iface.ObjectBase.PROPERTY_ID:
                    return "Id";
                case Iface.ObjectBase.PROPERTY_TYPEID:
                    return "TypeId";
            }

            return null;
        }

        private System.Reflection.MethodInfo methodEnumerable_Any;
        protected Expression ParsePropertyQuery(Iface.SQuery query, ParameterExpression param)
        {
            if (query is Iface.QValue) {
                var qvalue = (Iface.QValue)query;

                Expression xCompareFunc = null;
                Expression xComparedValue = null;

                MemberExpression xPropCollection = null;
                ParameterExpression xPropParam = null;
                BinaryExpression xPropNameEq = null;
                System.Reflection.MethodInfo methodGenericAny = null;

                string name = GetDirectField(qvalue.PropertyName);
                bool useProperty = name == null;

                if (useProperty) {
                    if (qvalue is Iface.QBool) {
                        xPropCollection = Expression.PropertyOrField(param, "BooleanProperties");
                        xPropParam = Expression.Parameter(typeof(DBRepo.BooleanProperty), "b");
                        xComparedValue = Expression.PropertyOrField(xPropParam, "Value");
                        methodGenericAny = methodEnumerable_Any.MakeGenericMethod(typeof(DBRepo.BooleanProperty));
                    } else if (qvalue is Iface.QDate) {
                        xPropCollection = Expression.PropertyOrField(param, "DateTimeProperties");
                        xPropParam = Expression.Parameter(typeof(DBRepo.DateTimeProperty), "d");
                        xComparedValue = Expression.PropertyOrField(xPropParam, "Value");
                        methodGenericAny = methodEnumerable_Any.MakeGenericMethod(typeof(DBRepo.DateTimeProperty));
                    } else {// if (qvalue is Repo.QString) { //default cari di string
                        xPropCollection = Expression.PropertyOrField(param, "StringProperties");
                        xPropParam = Expression.Parameter(typeof(DBRepo.StringProperty), "s");
                        xComparedValue = Expression.Call(Expression.PropertyOrField(xPropParam, "Value"), typeof(string).GetMethod("ToLower", new Type[0]));
                        methodGenericAny = methodEnumerable_Any.MakeGenericMethod(typeof(DBRepo.StringProperty));
                    }

                    xPropNameEq = Expression.Equal(Expression.PropertyOrField(xPropParam, "Name"), Expression.Constant(qvalue.PropertyName));
                } else {
                    xComparedValue = Expression.PropertyOrField(param, name);
                }

                if (qvalue.Mode == Iface.QValue.QMode.NULL) {
                    var xNull = Expression.Constant(null);
                    if (useProperty) {
                        return Expression.OrElse(
                            //property tidak ada
                            Expression.Not(Expression.Call(methodGenericAny, xPropCollection, Expression.Lambda(xPropNameEq, xPropParam))),
                            //property ada, value null
                            Expression.Call(methodGenericAny, xPropCollection, Expression.Lambda(Expression.AndAlso(xPropNameEq, Expression.Equal(xComparedValue, xNull)), xPropParam))
                            );
                    } else return Expression.Equal(xPropCollection, xNull);
                } else if (qvalue.Mode == Iface.QValue.QMode.NOT_NULL) {
                    xCompareFunc = Expression.Not(Expression.Equal(xComparedValue, Expression.Constant(null)));
                } else if (qvalue is Iface.QString) {
                    var xQueryVal = Expression.Constant(((Iface.QString)qvalue).Value);
                    if (qvalue.Mode == Iface.QString.QMode.EQUALS) {
                        xCompareFunc = Expression.Equal(xComparedValue, xQueryVal);
                    } else if (qvalue.Mode == Iface.QString.QMode.CONTAINS) {
                        xCompareFunc = Expression.Call(xComparedValue, typeof(string).GetMethod("Contains"), xQueryVal);
                    } else if (qvalue.Mode == Iface.QString.QMode.START_WITH) {
                        xCompareFunc = Expression.Call(xComparedValue, typeof(string).GetMethod("StartsWith"), xQueryVal);
                    } else if (qvalue.Mode == Iface.QString.QMode.END_WITH) {
                        xCompareFunc = Expression.Call(xComparedValue, typeof(string).GetMethod("EndsWith"), xQueryVal);
                    }
                } else if (qvalue is Iface.QBool) {
                    if (qvalue.Mode == Iface.QValue.QMode.FALSE) xCompareFunc = Expression.Equal(xComparedValue, Expression.Constant(false));
                    else xCompareFunc = Expression.Equal(xComparedValue, Expression.Constant(true));
                } else if (qvalue is Iface.QDate) {
                    var xQueryVal = Expression.Constant(((Iface.QString)qvalue).Value);
                    if (qvalue.Mode == Iface.QString.QMode.EQUALS) {
                        xCompareFunc = Expression.Equal(xComparedValue, xQueryVal);
                    } else if (qvalue.Mode == Iface.QString.QMode.GREATER_THAN) {
                        xCompareFunc = Expression.GreaterThan(xComparedValue, xQueryVal);
                    } else if (qvalue.Mode == Iface.QString.QMode.GREATER_EQUALS) {
                        xCompareFunc = Expression.GreaterThanOrEqual(xComparedValue, xQueryVal);
                    } else if (qvalue.Mode == Iface.QString.QMode.LESS_THAN) {
                        xCompareFunc = Expression.LessThan(xComparedValue, xQueryVal);
                    } else if (qvalue.Mode == Iface.QString.QMode.LESS_EQUALS) {
                        xCompareFunc = Expression.LessThanOrEqual(xComparedValue, xQueryVal);
                    }
                } else if (qvalue is Iface.QGuid) {
                    var xQueryVal = Expression.Constant(((Iface.QGuid)qvalue).Value.ToString());
                    if (qvalue.Mode == Iface.QGuid.QMode.EQUALS) {
                        xCompareFunc = Expression.Equal(xComparedValue, xQueryVal);
                    }
                }

                if (xCompareFunc != null) {
                    if (useProperty) {
                        var xAnyLmbd = Expression.Lambda(Expression.AndAlso(xPropNameEq, xCompareFunc), xPropParam);
                        return Expression.Call(methodGenericAny, xPropCollection, xAnyLmbd);
                    } else return xCompareFunc;
                }

            } else if (query is Iface.QAnd) {
                var qand = (Iface.QAnd)query;
                return Expression.AndAlso(ParsePropertyQuery(qand.Left, param), ParsePropertyQuery(qand.Right, param));
            } else if (query is Iface.QOr) {
                var qor = (Iface.QOr)query;
                return Expression.OrElse(ParsePropertyQuery(qor.Left, param), ParsePropertyQuery(qor.Right, param));
            }

            return Expression.Constant(true);
        }

        public override Iface.SearchResult SearchObject(Iface.SearchFileQuery query, ICollection<string> properties, Iface.SearchOrder[] order = null, int? skip = null, int page = int.MaxValue)
        {
            var result = new Iface.SearchResult() { };
            result.Files = new List<Iface.ObjectBase>();
            var dbquery = db.AliveFrom(db.Set<DBRepo.RepoObject>());
            
            //id
            if (query.RestrictIds != null) {
                dbquery = dbquery.Where(a => query.RestrictIds.Contains(a.Id));
            }

            //jika menggunakan tipeid
            if (query.TypeId != null) {
                dbquery = dbquery.Where(a => a.TypeId == query.TypeId);
            }

            //kalau di dalam folder
            if (query.InsideFolder != null) {
                dbquery = dbquery.Where(a => a.ParentFolders.Any(w => w.Id == query.InsideFolder.Id));
            }

            //list daftar methodinfo
            methodEnumerable_Any = typeof(Enumerable).GetMethods().Single(a => a.Name == "Any" && a.GetParameters().Length == 2);

            var xFileParam = Expression.Parameter(typeof(DBRepo.RepoObject), "o");
            var expBody = ParsePropertyQuery(query.PropertyQuery, xFileParam);
            dbquery = dbquery.Where(Expression.Lambda<Func<DBRepo.RepoObject, bool>>(expBody, xFileParam));

            //ambil total
            result.TotalWithoutPaging = dbquery.Count();

            //--- sort
            IOrderedQueryable<DBRepo.RepoObject> orderedQuery = null;
            if (order != null) {
                foreach (var o in order) {
                    if (o.Name == Iface.ObjectBase.PROPERTY_CREATED_ON) {
                        if (o.IsDescending) {
                            if (orderedQuery == null) dbquery = orderedQuery = dbquery.OrderByDescending(a => a.CreatedOn);
                            else orderedQuery = orderedQuery.ThenByDescending(a => a.CreatedOn);
                        } else {
                            if (orderedQuery == null) dbquery = orderedQuery = dbquery.OrderBy(a => a.CreatedOn);
                            else orderedQuery = orderedQuery.ThenBy(a => a.CreatedOn);
                        }
                    } else if (o.Name == Iface.ObjectBase.PROPERTY_CREATED_BY) {
                        if (o.IsDescending) {
                            if (orderedQuery == null) dbquery = orderedQuery = dbquery.OrderByDescending(a => a.CreatedByUID);
                            else orderedQuery = orderedQuery.ThenByDescending(a => a.CreatedByUID);
                        } else {
                            if (orderedQuery == null) dbquery = orderedQuery = dbquery.OrderBy(a => a.CreatedByUID);
                            else orderedQuery = orderedQuery.ThenBy(a => a.CreatedByUID);
                        }
                    } else if (o.Name == Iface.ObjectBase.PROPERTY_LAST_MODIFIED_ON) {
                        if (o.IsDescending) {
                            if (orderedQuery == null) dbquery = orderedQuery = dbquery.OrderByDescending(a => a.LastUpdatedOn);
                            else orderedQuery = orderedQuery.ThenByDescending(a => a.LastUpdatedOn);
                        } else {
                            if (orderedQuery == null) dbquery = orderedQuery = dbquery.OrderBy(a => a.LastUpdatedOn);
                            else orderedQuery = orderedQuery.ThenBy(a => a.LastUpdatedOn);
                        }
                    } else if (o.Name == Iface.ObjectBase.PROPERTY_LAST_MODIFIED_BY) {
                        if (o.IsDescending) {
                            if (orderedQuery == null) dbquery = orderedQuery = dbquery.OrderByDescending(a => a.LastUpdatedByUID);
                            else orderedQuery = orderedQuery.ThenByDescending(a => a.LastUpdatedByUID);
                        } else {
                            if (orderedQuery == null) dbquery = orderedQuery = dbquery.OrderBy(a => a.LastUpdatedByUID);
                            else orderedQuery = orderedQuery.ThenBy(a => a.LastUpdatedByUID);
                        }
                    } else if (o.Name == Iface.ObjectBase.PROPERTY_ID) {
                        if (o.IsDescending) {
                            if (orderedQuery == null) dbquery = orderedQuery = dbquery.OrderByDescending(a => a.Id);
                            else orderedQuery = orderedQuery.ThenByDescending(a => a.Id);
                        } else {
                            if (orderedQuery == null) dbquery = orderedQuery = dbquery.OrderBy(a => a.Id);
                            else orderedQuery = orderedQuery.ThenBy(a => a.Id);
                        }
                    } else {
                        //properti lain
                        if (o.IsDescending) {
                            if (orderedQuery == null) dbquery = orderedQuery = dbquery.OrderByDescending(a => a.StringProperties.FirstOrDefault(s => s.Name == o.Name).Value);
                            else orderedQuery = orderedQuery.ThenByDescending(a => a.StringProperties.FirstOrDefault(s => s.Name == o.Name).Value);
                        } else {
                            if (orderedQuery == null) dbquery = orderedQuery = dbquery.OrderBy(a => a.StringProperties.FirstOrDefault(s => s.Name == o.Name).Value);
                            else orderedQuery = orderedQuery.ThenBy(a => a.StringProperties.FirstOrDefault(s => s.Name == o.Name).Value);
                        }
                    }
                }
            }
            if (orderedQuery != null) dbquery = orderedQuery;
            else dbquery = dbquery.OrderBy(a => a.CreatedOn);

            //--- skip & paging
            if (skip.HasValue) dbquery = dbquery.Skip(skip.Value).Take(page);
            else dbquery = dbquery.Take(page);

            var queryResult = dbquery.ToList();
            result.Files = new List<Iface.ObjectBase>();

            //wrap file
            foreach (var repoObject in queryResult) {
                Iface.ObjectBase eobject = DBRepo.TypeResolver.CreateInstance(repoObject);
                eobject.Id = repoObject.Id;
                eobject.CreatedOn = repoObject.CreatedOn;
                eobject.CreatedBy = repoObject.CreatedByUID;
                eobject.LastModifiedBy = repoObject.LastUpdatedByUID;
                eobject.LastModifiedOn = repoObject.LastUpdatedOn;
                foreach (var propertyName in properties) {
                    var prop = repoObject.StringProperties.FirstOrDefault(a => a.Name == propertyName);
                    if (prop != null) eobject.StringProperties[propertyName] = prop.Value;
                    else {
                        var boolProp = repoObject.BooleanProperties.FirstOrDefault(a => a.Name == propertyName);
                        if (boolProp != null) eobject.BoolProperties[propertyName] = boolProp.Value;
                        else {
                            var intProp = repoObject.IntProperties.FirstOrDefault(a => a.Name == propertyName);
                            if (intProp != null) eobject.IntProperties[propertyName] = intProp.Value;
                            else {
                                var dtProp = repoObject.DateTimeProperties.FirstOrDefault(a => a.Name == propertyName);
                                if (dtProp != null) eobject.DateTimeProperties[propertyName] = dtProp.Value;
                            }
                        }
                    }
                }
                result.Files.Add(eobject);
            }

            return result;
        }

        internal const string SPECIAL_FOLDER_DOCUMENTS = "Documents";
        internal const string SPECIAL_FOLDER_TEMPLATES = "Templates";
        internal const string SPECIAL_FOLDER_TEMPS = "Temps";
        internal const string SPECIAL_FOLDER_MAPS = "Maps";

        public override Iface.FolderBase GetDocumentsFolder(string username = null)
        {
            var specialName = (string.IsNullOrEmpty(username)) ?
                SPECIAL_FOLDER_DOCUMENTS :
                username + "/" + SPECIAL_FOLDER_DOCUMENTS;
            var folder = db.SpecialFolders.FirstOrDefault(a => a.SpecialName == specialName);
            if (folder == null) return null;
            return (Iface.FolderBase)FindObject(folder.Id);
        }

        public override Iface.FolderBase GetTemplatesFolder(string username = null)
        {
            var specialName = (string.IsNullOrEmpty(username)) ?
                SPECIAL_FOLDER_TEMPLATES :
                username + "/" + SPECIAL_FOLDER_TEMPLATES;
            var folder = db.SpecialFolders.FirstOrDefault(a => a.SpecialName == specialName);
            if (folder == null) return null;
            return (Iface.FolderBase)FindObject(folder.Id);
        }

        public override Iface.FolderBase GetTemporaryFolder(string username)
        {
            var specialName = (string.IsNullOrEmpty(username)) ?
                SPECIAL_FOLDER_TEMPS :
                username + "/" + SPECIAL_FOLDER_TEMPS;
            var folder = db.SpecialFolders.FirstOrDefault(a => a.SpecialName == specialName);
            if (folder == null) return null;
            return (Iface.FolderBase)FindObject(folder.Id);
        }

        public override Iface.FolderBase GetMapsFolder()
        {
            var folder = db.SpecialFolders.FirstOrDefault(a => a.SpecialName == SPECIAL_FOLDER_MAPS);
            if (folder == null) return null;
            return (Iface.FolderBase)FindObject(folder.Id);
        }

        public override void PrepareWorkspace(string username = null)
        {
            string prefix = string.IsNullOrEmpty(username) ? "" : username + "/";

            if (!db.SpecialFolders.Any(a => a.SpecialName == prefix + SPECIAL_FOLDER_DOCUMENTS))
                db.SpecialFolders.Add(new DBRepo.RepoSpecialFolder() {
                    SpecialName = prefix + SPECIAL_FOLDER_DOCUMENTS,
                    Name = SPECIAL_FOLDER_DOCUMENTS
                });

            if (!db.SpecialFolders.Any(a => a.SpecialName == prefix + SPECIAL_FOLDER_TEMPLATES))
                db.SpecialFolders.Add(new DBRepo.RepoSpecialFolder() {
                    SpecialName = prefix + SPECIAL_FOLDER_TEMPLATES,
                    Name = SPECIAL_FOLDER_TEMPLATES
                });

            if (!db.SpecialFolders.Any(a => a.SpecialName == prefix + SPECIAL_FOLDER_TEMPS))
                db.SpecialFolders.Add(new DBRepo.RepoSpecialFolder() {
                    SpecialName = prefix + SPECIAL_FOLDER_TEMPS,
                    Name = SPECIAL_FOLDER_TEMPS
                });

            if (!db.SpecialFolders.Any(a => a.SpecialName == prefix + SPECIAL_FOLDER_MAPS))
                db.SpecialFolders.Add(new DBRepo.RepoSpecialFolder() {
                    SpecialName = prefix + SPECIAL_FOLDER_MAPS,
                    Name = SPECIAL_FOLDER_MAPS
                });

            db.SaveChanges();
        }
    }
}
