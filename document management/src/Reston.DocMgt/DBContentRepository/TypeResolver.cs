﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reston.DocMgt.DBRepo
{
    public static class TypeResolver
    {
        private const string TYPEID_CONST_NAME = "TYPE_ID";
        private const string CONTENTTYPE_CONST_NAME = "CONTENT_TYPE";
        private static Dictionary<string, Type> TypeCache = new Dictionary<string, Type>();

        public static string GetTypeId(ObjectBase objectBase)
        {
            var typeid = objectBase.GetType().GetField(TYPEID_CONST_NAME);
            if (typeid != null) {
                var raw = typeid.GetRawConstantValue();
                if (raw is string) return (string)raw;
            }
            return null;
        }

        public static string GetContentType(FileBase fileBase)
        {
            var typeid = fileBase.GetType().GetField(CONTENTTYPE_CONST_NAME);
            if (typeid != null) {
                var raw = typeid.GetRawConstantValue();
                if (raw is string) return (string)raw;
            }

            return fileBase.ContentType;
        }

        /// <summary>
        /// Mengkonstruksi ulang ObjectBase dari object di database
        /// </summary>
        /// <param name="repoObject"></param>
        /// <returns></returns>
        public static ObjectBase CreateInstance(RepoObject repoObject)
        {
            if (repoObject.TypeId != null) {
                Type matchType;
                if (!TypeCache.TryGetValue(repoObject.TypeId, out matchType)) {
                    if (repoObject is RepoFile) {
                        //cari tipe yang turunan File
                        matchType = (from asm in AppDomain.CurrentDomain.GetAssemblies()
                                     from type in asm.GetTypes()
                                     where typeof(FileBase).IsAssignableFrom(type)
                                     && type.GetField(TYPEID_CONST_NAME) != null
                                     && type.GetField(TYPEID_CONST_NAME).GetRawConstantValue().Equals(repoObject.TypeId)
                                     select type).FirstOrDefault();
                    } else if (repoObject is RepoFolder) {
                        //cari tipe yang turunan folder
                        matchType = (from asm in AppDomain.CurrentDomain.GetAssemblies()
                                     from type in asm.GetTypes()
                                     where typeof(FolderBase).IsAssignableFrom(type)
                                     && type.GetField(TYPEID_CONST_NAME) != null
                                     && type.GetField(TYPEID_CONST_NAME).GetRawConstantValue().Equals(repoObject.TypeId)
                                     select type).FirstOrDefault();
                    }

                    //add to cache
                    if (matchType != null) TypeCache[repoObject.TypeId] = matchType;
                }

                if (matchType != null) {
                    return (FileBase)matchType.GetConstructor(new Type[0]).Invoke(null);
                }
            }

            //jika tidak ada tipe spesifik, kembalikan tipe dasarnya
            if (repoObject is RepoFile) {
                return new BinaryFile();
            } else {
                return new FolderBase();
            }
        }
    }
}
