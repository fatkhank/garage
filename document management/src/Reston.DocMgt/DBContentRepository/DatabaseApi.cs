﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Reston.DocMgt.DBRepo
{
    interface ICreationAuditable
    {
        DateTime CreatedOn { get; set; }
        Guid CreatedByUID { get; set; }
    }

    interface IModificationAuditable
    {
        Nullable<DateTime> LastUpdatedOn { get; set; }
        Guid? LastUpdatedByUID { get; set; }
    }

    interface ISoftDeleteable
    {
        Nullable<DateTime> DeletedOn { get; set; }
        Guid? DeletedByUID { get; set; }
        bool IsDeleted { get; set; }
    }

    public abstract class CreationAuditableBaseEntity : ICreationAuditable
    {
        [Required]
        public DateTime CreatedOn { get; set; }
        [Required]
        public Guid CreatedByUID { get; set; }
    }

    public abstract class CreationDeletionAuditableBaseEntity : CreationAuditableBaseEntity, ISoftDeleteable
    {
        public bool IsDeleted { get; set; }
        public Guid? DeletedByUID { get; set; }
        public Nullable<DateTime> DeletedOn { get; set; }
    }

    public abstract class CUDAuditableBaseEntity : CreationDeletionAuditableBaseEntity, IModificationAuditable
    {
        public Guid? LastUpdatedByUID { get; set; }
        public Nullable<DateTime> LastUpdatedOn { get; set; }
    }
}
