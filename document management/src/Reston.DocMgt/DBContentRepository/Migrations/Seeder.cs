using System.Data.Entity.Migrations;

namespace Reston.DocMgt.DBRepo.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal static class Seeder
    {
        const string SEEDER_USERNAME = "seeder";
        const string SEEDER_USER_PRINCIPAL = "User:" + SEEDER_USERNAME;
        const string VIEW_PERMISSION = "View";

        internal static void Seed(DBRepoEntities context)
        {
            context.SetUser(Guid.Empty);
            var repo = new DbBasedContentRepository(context);

            //setup repository
            repo.PrepareWorkspace();
            repo.PrepareWorkspace(SEEDER_USERNAME);
        }
    }
}
