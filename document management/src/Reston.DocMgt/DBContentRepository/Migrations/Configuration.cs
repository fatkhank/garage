using System.Data.Entity.Migrations;

namespace Reston.DocMgt.DBRepo.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Reston.DocMgt.DBRepo.DBRepoEntities>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"DBContentRepository\Migrations";
        }

        protected override void Seed(Reston.DocMgt.DBRepo.DBRepoEntities context)
        {
            Seeder.Seed(context);
        }
    }
}
