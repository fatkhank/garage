﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reston.DocMgt
{
    public class BinaryFile : FileBase
    {
        public byte[] Content { get; internal set; }
        public int ContentLength { get { return Content == null ? 0 : Content.Length; } }

        public override bool ReadContentFrom(Stream stream)
        {
            if (stream.Length > int.MaxValue) throw new OverflowException("File content is to long");
            using (var reader = new BinaryReader(stream)) {
                this.Content = reader.ReadBytes((int)stream.Length);
                return true;
            }
        }

        public override void WriteContentTo(Stream stream)
        {
            if (this.Content != null) {
                //tulis jika ada isi
                stream.Write(this.Content, 0, this.Content.Length);
            }
        }

        public void SetContentType(string contentType)
        {
            this.ContentType = contentType;
        }
    }
}
