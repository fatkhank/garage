﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reston.DocMgt
{
    public class EORestonDocFile : FileBase
    {
        public const string TYPE_ID = TYPE_ID_PREFIX + "resdocv1";
        public const string CONTENT_TYPE = "application/vnd.reston.resdocv1";


        public const string DATA_TYPE_TEXT = "Text";
        public const string DATA_TYPE_DATE = "Date";
        public const string DATA_TYPE_NUMBER = "Number";

        public string Content { get; set; }
        private Dictionary<string, string> _pageOption;
        public Dictionary<string, string> PageOptions
        {
            get { if (_pageOption == null)_pageOption = new Dictionary<string, string>(); return _pageOption; }
        }

        private Dictionary<string, DataDef> _data;
        public List<DataDef> GetData()
        {
            if (_data == null) {
                _data = new Dictionary<string, DataDef>();
            }
            return _data.Values.ToList();
        }

        public DataDef DefineData(string name, string type = DATA_TYPE_TEXT)
        {
            if (_data == null) _data = new Dictionary<string, DataDef>();

            return (_data[name] = new DataDef() {
                Name = name,
                Type = type
            });
        }

        public void SetDataValue(string name, object value, string type = null)
        {
            if (_data == null) _data = new Dictionary<string, DataDef>();

            DataDef def;
            if (!_data.TryGetValue(name, out def)) {
                if (type == null) {
                    if (value is DateTime) {
                        type = DATA_TYPE_DATE;
                    } else type = DATA_TYPE_TEXT;
                }

                def = DefineData(name, type);
            }

            if (def.Type == DATA_TYPE_TEXT) {
                def.Value = value == null ? "" : value.ToString();
            } else if (def.Type == DATA_TYPE_DATE) {
                def.Value = value == null ? "" : ((DateTime)value).ToString();
            }
        }

        public override void WriteContentTo(Stream stream)
        {
            using (var writer = new StreamWriter(stream)) {
                //page option
                var data = PageOptions;
                writer.WriteLine(data.Count());
                foreach (var d in data) {
                    if (d.Value != null) {
                        writer.WriteLine(d.Key + '=' + System.Text.RegularExpressions.Regex.Escape(d.Value == null ? "" : d.Value));
                    }
                }

                //param
                if (_data != null) {
                    var dataList = GetData();
                    writer.WriteLine(dataList.Count());
                    foreach (var d in dataList) {
                        if (d.Value != null) {
                            var key = d.Name;
                            if (d.Type != null) key += ":" + d.Type;

                            writer.WriteLine(key + '=' + System.Text.RegularExpressions.Regex.Escape(d.Value == null ? "" : d.Value));
                        }
                    }
                } else {
                    writer.WriteLine('0');
                }

                //content
                writer.Write(Content);
            }
        }

        public override bool ReadContentFrom(Stream stream)
        {
            using (StreamReader reader = new StreamReader(stream)) {
                //read option
                int paramCount;
                if (!int.TryParse(reader.ReadLine(), out paramCount)) return false;
                if (paramCount > 0) _pageOption = new Dictionary<string, string>();
                for (var l = 0; l < paramCount; l++) {
                    var line = reader.ReadLine();
                    var key = line.Substring(0, line.IndexOf('='));
                    var value = line.Substring(key.Length + 1);
                    _pageOption[key] = string.IsNullOrEmpty(value) ? null : System.Text.RegularExpressions.Regex.Unescape(value);
                };

                //read data
                if (!int.TryParse(reader.ReadLine(), out paramCount)) return false;
                if (paramCount > 0) {
                    _data = new Dictionary<string, DataDef>();
                    for (var l = 0; l < paramCount; l++) {
                        var line = reader.ReadLine();
                        var varDef = line.Substring(0, line.IndexOf('='));

                        var varName = varDef.Substring(0, varDef.LastIndexOf(':'));
                        var varType = varDef.Substring(varName.Length + 1);

                        var current = _data[varName] = new DataDef() {
                            Name = varName,
                            Type = varType
                        };

                        var value = line.Substring(varDef.Length + 1);
                        //                        if (varType == DATA_TYPE_TEXT) {
                        current.Value = string.IsNullOrEmpty(value) ? null : System.Text.RegularExpressions.Regex.Unescape(value);
                        //}
                    };
                }

                //read content
                this.Content = reader.ReadToEnd();
                return true;
            }
        }

        public class DataDef
        {
            public string Name { get; set; }
            public string Type { get; set; }
            public string Value { get; set; }
        }
    }
}
