﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Reston.DocMgt
{
    public class EODocumentFile : FileBase
    {
        public const string TYPE_ID = TYPE_ID_PREFIX + "eodoc";
        public const string CONTENT_TYPE = "application/vnd.reston.eodocv1";
        private const string PROPERTY_PREFIX = "reo:eodoc:";

        public EODocumentFile() {}

        public Guid? MainContentId { get; private set; }

        public void SetMainContent(ObjectBase content) { this.MainContentId = content.Id; }

        private List<Attachment> Attachments { get; set; }

        public void AddAttachment(ObjectBase eobject, string label)
        {
            if (Attachments == null) Attachments = new List<Attachment>();
            //            if (Attachments.Contains(eobject.Id)) return;
            Attachments.Add(new Attachment() {
                Id = eobject.Id,
                Label = label
            });
        }

        public void ClearAttachment() { if (Attachments != null) Attachments.Clear(); }

        public Attachment[] GetAttachments()
        {
            return this.Attachments == null ? new Attachment[0] : this.Attachments.ToArray();
        }

        #region Properties
        public const string PROPERTY_SUBJECT = PROPERTY_PREFIX + "Subject";
        public string Subject
        {
            get { return StringProperties.ContainsKey(PROPERTY_SUBJECT) ? StringProperties[PROPERTY_SUBJECT] : null; }
            set { this.StringProperties[PROPERTY_SUBJECT] = value; }
        }

        public const string PROPERTY_NUMBER = PROPERTY_PREFIX + "Number";
        public string Number
        {
            get { return StringProperties.ContainsKey(PROPERTY_NUMBER) ? StringProperties[PROPERTY_NUMBER] : null; }
            set { this.StringProperties[PROPERTY_NUMBER] = value; }
        }

        public const string PROPERTY_CLASSIFICATION = PROPERTY_PREFIX + "Classification";
        public string Classification
        {
            get { return StringProperties.ContainsKey(PROPERTY_CLASSIFICATION) ? StringProperties[PROPERTY_CLASSIFICATION] : null; }
            set { this.StringProperties[PROPERTY_CLASSIFICATION] = value; }
        }

        public const string PROPERTY_PRIORITY = PROPERTY_PREFIX + "Priority";
        public string Priority
        {
            get { return StringProperties.ContainsKey(PROPERTY_PRIORITY) ? StringProperties[PROPERTY_PRIORITY] : null; }
            set { this.StringProperties[PROPERTY_PRIORITY] = value; }
        }

        public const string PROPERTY_VARIETY = PROPERTY_PREFIX + "Variety";
        public string Variety
        {
            get { return StringProperties.ContainsKey(PROPERTY_VARIETY) ? StringProperties[PROPERTY_VARIETY] : null; }
            set { this.StringProperties[PROPERTY_VARIETY] = value; }
        }

        public const string PROPERTY_IS_CLASSIFIED = PROPERTY_PREFIX + "Classified";
        public bool IsClassified
        {
            get
            {
                return BoolProperties.ContainsKey(PROPERTY_IS_CLASSIFIED) ?
                    BoolProperties[PROPERTY_IS_CLASSIFIED] : false;
            }
            set { this.BoolProperties[PROPERTY_IS_CLASSIFIED] = value; }
        }


        public const string PROPERTY_ORIGIN = PROPERTY_PREFIX + "Origin";
        public string Origin
        {
            get { return StringProperties.ContainsKey(PROPERTY_ORIGIN) ? StringProperties[PROPERTY_ORIGIN] : null; }
            set { this.StringProperties[PROPERTY_ORIGIN] = value; }
        }

        public const string PROPERTY_IS_EXTERNAL = PROPERTY_PREFIX + "IsExternal";
        public bool IsExternal
        {
            get { return BoolProperties.ContainsKey(PROPERTY_IS_EXTERNAL) ? BoolProperties[PROPERTY_IS_EXTERNAL] : false; }
            set { this.BoolProperties[PROPERTY_IS_EXTERNAL] = value; }
        }

        public const string PROPERTY_RECEIEVEON = PROPERTY_PREFIX + "ReceiveOn";
        public DateTime? RecievedOn
        {
            get { return DateTimeProperties.ContainsKey(PROPERTY_RECEIEVEON) ? DateTimeProperties[PROPERTY_RECEIEVEON] : (DateTime?)null; }
            set { if (value.HasValue)this.DateTimeProperties[PROPERTY_RECEIEVEON] = value.Value; }
        }

        public const string PROPERTY_OWNER = PROPERTY_PREFIX + "Owner";
        public Guid Owner
        {
            get { return StringProperties.ContainsKey(PROPERTY_OWNER) ? Guid.Parse(StringProperties[PROPERTY_OWNER]) : Guid.Empty; }
            set { this.StringProperties[PROPERTY_OWNER] = value.ToString(); }
        }

        #endregion


        //        [Serializable]
        public class EODocumentContent
        {
            public Guid? MainContentId { get; set; }
            public Attachment[] Attachments { get; set; }
        }

        //        [Serializable]
        public class Attachment
        {
            [XmlAttribute]
            public string Label { get; set; }

            [XmlAttribute]
            public Guid Id { get; set; }
        }

        public override bool ReadContentFrom(Stream stream)
        {
            //--- binary
            //var formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            //var content = (EODocumentContent)formatter.Deserialize(stream);
            //--- xml
            var xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(EODocumentContent));
            var content = (EODocumentContent)xmlSerializer.Deserialize(stream);

            //ekstrak dari content
            MainContentId = content.MainContentId;
            Attachments = new List<Attachment>(content.Attachments);

            return true;
        }

        public override void WriteContentTo(Stream stream)
        {
            var content = new EODocumentContent() {
                MainContentId = MainContentId,
                Attachments = GetAttachments()
            };

            //--- direct
            //var formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            //formatter.Serialize(stream, content);
            //--- xml
            var xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(EODocumentContent));
            xmlSerializer.Serialize(stream, content);
        }
    }

    public class EODocumentTemplate : EODocumentFile
    {
        new public const string TYPE_ID = TYPE_ID_PREFIX + "eodocTemplate";
        new public const string CONTENT_TYPE = "application/vnd.reston.eodocv1-template";
    }
}
