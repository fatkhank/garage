"use strict";

/**
 * url
 * opt : Option
 * - expire_time    :   waktu expire tiap entry
 * - fetch(key)     :   fungsi untuk mengambil data entry
 * - default        : default nilai entry jika tidak ditemukan
 */
var LocalCache = function (name, opt) {
    var _ = this;

    _.option = opt;
    _.name = name;

    /**
     * Data cache holder. Struktur masing2 entry:
     * - data  : nilai entry
     * - queue  : antrian callback
     * - exp    : tanggal expire entry
     */
    _.db = {};

    /**
     * Baca dari penyimpanan local
     */
    _.readLocal = function () {
        //baca dari local storage
        _.db = JSON.parse(localStorage.getItem(_.name)) || {};
        //bersihkan data
        for (var u in _.db) {
            var uc = _.db[u];
            uc.queue = [];
            uc.exp = new Date(uc.exp);
        };
    };

    /**
     * Simpan ke penyimpanan local
     */
    _.saveLocal = function () {
        localStorage.setItem(_.name, JSON.stringify(_.db));
    };

    /**
     * Mendapatkan cache untuk key tertentu.
     * - key
     * - callback   : fungsi jika entry sudah didapat
     *                function(result, key)
     * - force      : paksa fetch entry
     */
    _.get = function (key, callback, force) {
        var entry = _.db[key];

        if (!entry || force) _.fetch(key, callback);
        else if (entry.queue.length) _.db[key].queue.push(callback); //jika sudah diminta sebelumnya, antrikan
        else if (entry.exp < new Date()) _.fetch(key, callback);//expired
        else callback(entry.data, key);
    };

    /**
     * fetch data 
     */
    _.fetch = function (key, callback) {
        var entry = { queue: [callback] };
        _.db[key] = entry;

        var promise = opt.fetch(key);

        //jika sukses
        promise.done = function (data) {
            entry.data = data;

            executeCallback(key);

            //bersihkan queue
            entry.queue = [];

            //atur expiracy
            var d = new Date();
            d.setMinutes(d.getMinutes() + opt.expire_time);
            entry.exp = d;

            _.saveLocal();
        }

        //jika gagal
        promise.fail = function () {
            entry.data = opt.default;
            executeCallback(key);
        };

        function executeCallback(key) {
            var uc = _.db[key];
            var q = uc.queue;
            for (var i = 0; i < q.length; i++) {
                q[i](uc.data, key);
            }
        };


    };
}