﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace CodeFirstWithAuditTrail
{
    public partial class ModelContext : DbContext
    {
        /// <summary>
        /// Nama skema untuk database
        /// </summary>
        public const string SCHEMA_NAME = "custom";

        public ModelContext(string nameOrConnectionString) : base(nameOrConnectionString) { }

        public virtual DbSet<ReferenceData> ReferenceData { get; set; }


        private Guid accessorUID { get; set; }

        /// <summary>
        /// Cantumkan nama user yang sedang mengakses entiti.
        /// </summary>
        /// <param name="username"></param>
        public void SetUser(Guid username)
        {
            accessorUID = username;
        }

        public bool SoftDeleteOff { get; set; }

        /// <summary>
        /// Menyimpan perubahan ke database. Jika terdapat perubahan di ReferenceData, pastikan <code>ValidateReferenceData</code> dipanggil terlebih dahulu.
        /// </summary>
        /// <returns></returns>
        public override int SaveChanges()
        {
            //cantumkan audit trail untuk entity yang ICreationAuditable
            foreach (var entry in this.ChangeTracker.Entries<ICreationAuditable>().Where(a => a.State == EntityState.Added)) {
                entry.Entity.CreatedByUID = accessorUID;
                entry.Entity.CreatedOn = DateTime.Now;
            }

            //cantumkan audit trail untuk entity yang IModificationAuditable
            foreach (var entry in this.ChangeTracker.Entries<IModificationAuditable>().Where(a => a.State == EntityState.Modified)) {
                entry.Entity.LastUpdatedByUID = accessorUID;
                entry.Entity.LastUpdatedOn = DateTime.Now;
            }

            if (!SoftDeleteOff) {
                //cantumkan audit trail untuk entity yang ISoftDeletable
                foreach (var entry in this.ChangeTracker.Entries<ISoftDeleteable>().Where(a => a.State == EntityState.Deleted)) {
                    entry.Entity.DeletedByUID = accessorUID;
                    entry.Entity.DeletedOn = DateTime.Now;
                    entry.Entity.IsDeleted = true;
                    entry.State = EntityState.Modified;
                }
            }
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(System.Threading.CancellationToken cancellationToken)
        {
            //cantumkan audit trail untuk entity yang ICreationAuditable
            foreach (var entry in this.ChangeTracker.Entries<ICreationAuditable>().Where(a => a.State == EntityState.Added)) {
                entry.Entity.CreatedByUID = accessorUID;
                entry.Entity.CreatedOn = DateTime.Now;
            }

            //cantumkan audit trail untuk entity yang IModificationAuditable
            foreach (var entry in this.ChangeTracker.Entries<IModificationAuditable>().Where(a => a.State == EntityState.Modified)) {
                entry.Entity.LastUpdatedByUID = accessorUID;
                entry.Entity.LastUpdatedOn = DateTime.Now;
            }

            if (!SoftDeleteOff) {
                //cantumkan audit trail untuk entity yang ISoftDeletable
                foreach (var entry in this.ChangeTracker.Entries<ISoftDeleteable>().Where(a => a.State == EntityState.Deleted)) {
                    entry.Entity.DeletedByUID = accessorUID;
                    entry.Entity.DeletedOn = DateTime.Now;
                    entry.Entity.IsDeleted = true;
                    entry.State = EntityState.Modified;
                }
            }

            return base.SaveChangesAsync(cancellationToken);
        }

        /// <summary>
        /// Melakukan validasi terhadap reference data untuk menghindari bentrok kode.
        /// Method ini harus dijalankan sebelum SaveChange() jika terdapat penambahan atau perubahan terhadap ReferenceData.
        /// </summary>
        public void ValidateReferenceData()
        {
            //Qualifier dan Code tidak bisa diubah
            foreach (var entry in this.ChangeTracker.Entries<ReferenceData>().Where(a => a.State == EntityState.Modified)) {
                entry.Property(a => a.Qualifier).IsModified = false;
                entry.Property(a => a.Code).IsModified = false;
            }

            //untuk reference data yang baru dimasukkan, cek reference data
            foreach (var entry in this.ChangeTracker.Entries<ReferenceData>().Where(a => a.State == EntityState.Added)) {
                //cari yang qualifier dan kodenya sama
                var duplicates = this.ReferenceData.Where(a =>
                    a.Qualifier == entry.Entity.Qualifier
                    && a.Code == entry.Entity.Code).ToList();

                //gunakan prefix untuk menghindari duplikat pada kode yang sudah dihapus
                if (duplicates.Count > 0) {
                    entry.Entity.Code += duplicates.Count;
                }
            }
        }

        /// <summary>
        /// Menyaring dari dbset yang belum dihapus
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dbset"></param>
        /// <returns></returns>
        public IQueryable<T> AliveFrom<T>(DbSet<T> dbset) where T : CreationDeletionAuditableBaseEntity
        {
            return dbset.Where(a => !a.IsDeleted);
        }

        public IQueryable<T> AliveFrom<T>(Func<ModelContext, DbSet<T>> dbset) where T : CreationDeletionAuditableBaseEntity
        {
            return dbset(this).Where(a => !a.IsDeleted);
        }

        /// <summary>
        /// Mencari entry dalam set yang belum dihapus
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dbset"></param>
        /// <param name="keyValues"></param>
        /// <returns></returns>
        public T FindAlive<T>(Func<ModelContext, DbSet<T>> dbset, params object[] keyValues) where T : CreationDeletionAuditableBaseEntity
        {
            T entry = dbset(this).Find(keyValues);
            if (entry == null || entry.IsDeleted) return default(T);//jika tidak ketemu atau sudah dihapus, kembalikan null
            else return entry;
        }

        public async Task<T> FindAliveAsync<T>(Func<ModelContext, DbSet<T>> dbset, params object[] keyValues) where T : CreationDeletionAuditableBaseEntity
        {
            T entry = await dbset(this).FindAsync(keyValues);
            if (entry == null || entry.IsDeleted) return default(T);//jika tidak ketemu atau sudah dihapus, kembalikan null
            else return entry;
        }
    }
}
