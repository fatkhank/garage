﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CodeFirstWithAuditTrail
{
    internal interface ICreationAuditable
    {
        DateTime CreatedOn { get; set; }
        Guid CreatedByUID { get; set; }
    }

    internal interface IModificationAuditable
    {
        DateTime? LastUpdatedOn { get; set; }
        Nullable<Guid> LastUpdatedByUID { get; set; }
    }

    internal interface ISoftDeleteable
    {
        DateTime? DeletedOn { get; set; }
        Nullable<Guid> DeletedByUID { get; set; }
        bool IsDeleted { get; set; }
    }





    /// <summary>
    /// Entity dasar yang data penambahan baris tercatat.
    /// </summary>
    /// <typeparam name="T">Tipe id aktor</typeparam>
    public abstract class CreationAuditableBaseEntity : ICreationAuditable
    {
        [Required]
        public DateTime CreatedOn { get; set; }
        [Required]
        public Guid CreatedByUID { get; set; }
    }

    /// <summary>
    /// Entity dasar yang data penambahan/perubahan baris tercatat.
    /// </summary>
    /// <typeparam name="T">Tipe id aktor</typeparam>
    public abstract class CreationDeletionAuditableBaseEntity : CreationAuditableBaseEntity, ISoftDeleteable
    {
        public bool IsDeleted { get; set; }
        public Guid? DeletedByUID { get; set; }
        public DateTime? DeletedOn { get; set; }
    }

    /// <summary>
    /// Entity dasar yang data penambahan, perubahan dan penghapusan baris tercatat.
    /// </summary>
    /// <typeparam name="T">Tipe id aktor</typeparam>
    public abstract class CUDAuditableBaseEntity : CreationDeletionAuditableBaseEntity, IModificationAuditable
    {
        public Guid? LastUpdatedByUID { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
    }

    /// <summary>
    /// Menandakan bahwa keterangan enum harus di simpan di reference data
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Enum, AllowMultiple = false)]
    internal class RefDataAttribute : System.Attribute
    {
        /// <summary>
        /// Nama qualifier
        /// </summary>
        public string Qualifier { get; private set; }
        public RefDataAttribute(string qualifier)
        {
            this.Qualifier = qualifier;
        }
    }

    /// <summary>
    /// Menandakan property tambahan yang perlu disimpan di reference data
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = false)]
    internal class RefDataEntry : System.Attribute
    {
        public string Desc { get; set; }
        public string Name { get; set; }
        public string StringAttr { get; set; }
        public int IntAttr { get; set; }
    }
}
