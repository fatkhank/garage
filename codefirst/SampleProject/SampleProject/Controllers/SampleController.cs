﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SampleProject.Controllers
{
    public class SampleController : Controller
    {
        public ActionResult DoSomething(string param1, int param2 = 5)
        {
            return Content("You request: param1=" + param1 + " and param2="+param2);
        }
    }
}