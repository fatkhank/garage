﻿using SampleProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SampleProject.Controllers
{
    public class RegistrationController : Controller
    {
        /// <summary>
        /// Form add person
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AddForm() {
            return View();
        }

        /// <summary>
        /// Create new Person
        /// </summary>
        /// <param name="name"></param>
        /// <param name="gender"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Submit(Person model) {
            //save person
            return RedirectToAction("Index","Home");
        }

        /// <summary>
        /// Menghapus seseorang
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public ActionResult Delete(int id) {
            //do something
            return View();
        }
    }
}