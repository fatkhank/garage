﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SampleProject.Controllers
{
    public class CarController : Controller
    {
        public void NoReturn() {
            //do something
        }

        public ActionResult GetText()
        {
            return Content("");
        }

        public ActionResult GetView() {
            return View();
        }

        public ActionResult GetJson() {
            return Json(new {total=5,  });
        }

        public ActionResult GetFile()
        {
            byte[] fileContent = new byte[3];
            return File(fileContent, "text/plain", "sample_file.txt");
        }
    }
}