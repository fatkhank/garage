﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SampleProject.Models
{
    public class Person
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MinLength(20)]
        public string Name { get; set; }

        public string Gender { get; set; }

        public List<Address> Addresses { get; set; }
    }

    public class Address
    {
        public string AddressText { get; set; }
        public string PostCode { get; set; }
        public string City { get; set; }
    }
}